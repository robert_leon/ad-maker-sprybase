// Get DOM Elements
const modal = document.querySelector('#my-modal');
const modalBtn = document.getElementById('modal-btn');
const closeBtn = document.querySelector('.close');
const cancelBtn = document.querySelector('.btn-cancel');
const number = document.querySelectorAll('input[type="tel"]');
const inputs = document.querySelectorAll('input');
const files = document.querySelectorAll('file');
const textareas = document.querySelectorAll('textarea');
const dropDowns = document.querySelectorAll('select');
//end of basics
const price = document.querySelector('.price .textbox-container input[type="tel"]');

// Events
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
cancelBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);
for (var i = 0; i < number.length; i++) {
  number[i].addEventListener('input', letOnlyNumbers, false);
}
for (var i = 0; i < inputs.length; i++) {
  if(inputs[i].getAttribute("type")!="file"){
    inputs[i].addEventListener('focusout', checkBlanks, false);
    inputs[i].addEventListener('focusin', activateHelpers, false);
  } 
}
for (var i = 0; i < textareas.length; i++) {
  textareas[i].addEventListener('focusout', checkBlanks, false);
  textareas[i].addEventListener('focusin', activateHelpers, false);
}
for (var i = 0; i < dropDowns.length; i++) {
  dropDowns[i].addEventListener('focusout', checkBlanks, false);
}


// Generate apartment type
apartment = document.getElementById('apartment-type');
var array = ["1 room", "2 rooms", "3 rooms"];
for (var i = 0; i < array.length; i++) {
  var option = document.createElement("option");
  option.text = array[i];
  apartment.add(option);
}

// Open
function openModal() {
  modal.style.display = 'block';
}

// Close
function closeModal() {
  modal.style.display = 'none';
}

// Close If Outside Click
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = 'none';
  }
}

// All tel type fields accept only numbers
function letOnlyNumbers() {

  this.value = this.value.replace(/[^0-9]/g, '');
  //can't start with 0 by uncommenting this:
  //this.value=this.value.replace(/^0/g,'');
}
// Closer fallback for IE11
var getClosest = function (elem, selector) {

  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function (s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) { }
        return i > -1;
      };
  }

  // Get the closest matching element
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }
  return null;

};
// Activate helpers
function activateHelpers() {
  if (!this.classList.contains("invalid")) {
    getClosest(this, ".row").querySelector('.help-text').classList.remove("hide");
    // this.closest(".row").querySelector('.help-text').classList.remove("hide");
  }
}

// Validate fields not to be empty
function checkBlanks() {
  let text = "Please complete the field";
  if (this.value == "") {
    if (getClosest(this, ".row").classList.contains('dropdown')) {
      text = "Please select option from dropdown";
    }
    else{
      if (getClosest(this, ".row").classList.contains('number')) {
        text = "Please enter a number";
      }
      else if (getClosest(this, ".row").classList.contains('file')) {
        text = "Please add the image";
      }
    }
    getClosest(this, ".row").querySelector('.error-text').innerHTML = text;
    this.classList.add("invalid");
    getClosest(this, ".row").querySelector('.help-text').classList.add("hide");
  }
  else {
    getClosest(this, ".row").querySelector('.error-text').innerHTML = "";
    this.classList.remove("invalid");
    getClosest(this, ".row").querySelector('.help-text').classList.add("hide");
    validatePrice();
  }
}

// Validate price when field is completed
function validatePrice() {

  // if(price.value.startsWith("0")){
  if (price.value.indexOf("0") == 0) {
    document.querySelector(".price").querySelector('.error-text').innerHTML = "Price number can't start with 0.";
    price.classList.add("invalid");
  }
  else if (!price.value == "") {
    document.querySelector(".price").querySelector('.error-text').innerHTML = "";
    price.classList.remove("invalid");
  }
}
//
function cheeckFields(y) {
  let text = "Please complete the field";
  if (y.value == "" && !getClosest(y, ".row").classList.contains('hide')) {
    if (getClosest(y, ".row").classList.contains('dropdown')) {
      text = "Please select option from dropdown";
    }
    else{
      if (getClosest(y, ".row").classList.contains('number')) {
        text = "Please enter a number";
      }
      else if(getClosest(y, ".row").classList.contains('file')){
        text = "Please add a picture";
      }
    }
    getClosest(y, ".row").querySelector('.error-text').innerHTML = text;
    y.classList.add("invalid");
    getClosest(y, ".row").querySelector('.help-text').classList.add("hide");
  }
  else {
    getClosest(y, ".row").querySelector('.error-text').innerHTML = "";
    y.classList.remove("invalid");
    getClosest(y, ".row").querySelector('.help-text').classList.add("hide");
    validatePrice();
  }
}
// Submit action
function submit() {
  for (var i = 0; i < inputs.length; i++) {
    cheeckFields(inputs[i]);
  }
  for (var i = 0; i < dropDowns.length; i++) {
    cheeckFields(dropDowns[i]);
  }
  for (var i = 0; i < textareas.length; i++) {
    cheeckFields(textareas[i]);
  }
}
//add more files
// function addMore() {
//   var x = document.createElement("input");
//   x.setAttribute("type", "file");
//   x.setAttribute("multiple",'')
//   x.setAttribute("accept","image/*");
//   document.querySelector(".upload .textbox-container").appendChild(x);
// }