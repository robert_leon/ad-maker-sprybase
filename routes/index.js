var express = require('express');
var router = express.Router();
var fs = require('fs');


/* GET home page. */
router.get('/', function(req, res, next) {
  let rawdata = fs.readFileSync("./public/database.json");
  let results = JSON.parse(rawdata);
  var button = "Add your ad";
  if(results.title !== undefined){
     button = "Change your ad";
     res.render('index-success', { title: 'Sign Up', button: button, announcementtitle: results.title, description: results.description,
                      apartment: results.apartment, image1: " ./public/images/"+ results.image1, image2: results.image2, adress: results.adress,
                      price: results.price, phonenumber: results.phonenumber });
  }
  else{
    res.render('index', { title: 'Sign Up'});
  }
});

router.post('/add', function(req, res, next){
  var obj = {
    title: req.body.announcementtitle,
    description: req.body.description,
    apartment: req.body.apartment,
    image1: req.body.image1,
    image2: req.body.image2,
    adress: req.body.adress,
    price: req.body.price,
    phonenumber: req.body.phonenumber
  };
  fs.writeFile ("./public/database.json", JSON.stringify(obj), function(err) {
    if (err) throw err;
    console.log('complete');
    }
  );
  res.redirect('/');  
});

router.post('/delete', function(req, res, next){
  var obj = {
  };
  fs.writeFile ("./public/database.json", JSON.stringify(obj), function(err) {
    if (err) throw err;
    console.log('complete');
    }
  );
  res.redirect('/');  
});
module.exports = router;
